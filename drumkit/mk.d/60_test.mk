SUITE ?= default
BEHAT_SUITE = --suite='$(SUITE)'
ifdef TAGS
    BEHAT_TAGS = --tags='$(TAGS)'
endif

run-behat: fixtures
	@$(DDEV) behat $(BEHAT_TAGS) $(BEHAT_SUITE)

wip: run-behat-wip

fixtures:
	@$(DRUSH) pm-enable -y aegir_input_command_test aegir_log_command_test

run-behat-wip: fixtures
	@$(DDEV) behat $(BEHAT_SUITE) --tags="@wip&&~@disabled"

tests: run-behat # run-simpletest ##@aegir Ægir: Run Behat-based tests
