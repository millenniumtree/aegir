<?php

namespace Drupal\aegir_example\Entity;

use Drupal\aegir_api\Entity\AbstractEntityType;

/**
 * Defines an Ægir example type entity.
 *
 * @ConfigEntityType(
 *   id = "aegir_example_type",
 *   label = @Translation("Example type"),
 *   handlers = {
 *     "list_builder" = "Drupal\aegir_api\Entity\EntityType\ListBuilderBase",
 *     "form" = {
 *       "add" = "Drupal\aegir_api\Entity\EntityType\Form\EntityTypeFormBase",
 *       "edit" = "Drupal\aegir_api\Entity\EntityType\Form\EntityTypeFormBase",
 *       "delete" = "Drupal\aegir_api\Entity\EntityType\Form\DeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_api\Entity\EntityType\HtmlRouteProviderBase",
 *     },
 *   },
 *   config_prefix = "aegir_example_type",
 *   admin_permission = "administer aegir example types",
 *   bundle_of = "aegir_example",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/examples/types/{aegir_example_type}",
 *     "add-form" = "/admin/aegir/examples/types/add",
 *     "edit-form" = "/admin/aegir/examples/types/{aegir_example_type}/edit",
 *     "delete-form" = "/admin/aegir/examples/types/{aegir_example_type}/delete",
 *     "collection" = "/admin/aegir/examples/types"
 *   }
 * )
 */
class EntityType extends AbstractEntityType {

}
