<?php

namespace Drupal\aegir_example\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionRevertTranslationForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Ægir example revision for a translation.
 *
 * @ingroup aegir_example
 */
class RevisionRevertTranslationForm extends AbstractRevisionRevertTranslationForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.field_manager')->getStorage('aegir_example'),
      $container->get('date.formatter'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_example_revision_revert_translation_confirm';
  }

}
