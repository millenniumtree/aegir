<?php

namespace Drupal\aegir_example\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionRevertForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Ægir Example revision.
 *
 * @ingroup aegir_example
 */
class RevisionRevertForm extends AbstractRevisionRevertForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.field_manager')->getStorage('aegir_example'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_example_revision_revert_confirm';
  }

}
