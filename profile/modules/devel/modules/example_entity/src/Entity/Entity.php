<?php

namespace Drupal\aegir_example\Entity;

use Drupal\aegir_api\Entity\AbstractEntity;

/**
 * Defines an Ægir example entity.
 *
 * @ingroup aegir_example
 *
 * @ContentEntityType(
 *   id = "aegir_example",
 *   label = @Translation("Example"),
 *   bundle_label = @Translation("Example type"),
 *   handlers = {
 *     "storage" = "Drupal\aegir_api\Entity\StorageBase",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\ListBuilderBase",
 *     "views_data" = "Drupal\aegir_api\Entity\ViewsDataBase",
 *     "translation" = "Drupal\aegir_api\Entity\TranslationHandlerBase",
 *
 *     "form" = {
 *       "default" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "add" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "edit" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "delete" = "Drupal\aegir_api\Entity\Form\DeleteFormBase",
 *     },
 *     "access" = "Drupal\aegir_api\Entity\AccessControlHandlerBase",
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_api\Entity\HtmlRouteProviderBase",
 *     },
 *   },
 *   base_table = "aegir_example",
 *   data_table = "aegir_example_field_data",
 *   revision_table = "aegir_example_revision",
 *   revision_data_table = "aegir_example_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer aegir example entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/examples/{aegir_example}",
 *     "add-page" = "/admin/aegir/examples/add",
 *     "add-form" = "/admin/aegir/example/add/{aegir_example_type}",
 *     "edit-form" = "/admin/aegir/examples/{aegir_example}/edit",
 *     "delete-form" = "/admin/aegir/examples/{aegir_example}/delete",
 *     "version-history" = "/admin/aegir/examples/{aegir_example}/revisions",
 *     "revision" = "/admin/aegir/examples/{aegir_example}/revisions/{entity_revision}/view",
 *     "revision_revert" = "/admin/aegir/examples/{aegir_example}/revisions/{entity_revision}/revert",
 *     "translation_revert" = "/admin/aegir/examples/{aegir_example}/revisions/{entity_revision}/revert/{langcode}",
 *     "revision_delete" = "/admin/aegir/examples/{aegir_example}/revisions/{entity_revision}/delete",
 *     "collection" = "/admin/aegir/examples",
 *   },
 *   bundle_entity_type = "aegir_example_type",
 *   field_ui_base_route = "entity.aegir_example_type.canonical"
 * )
 */
class Entity extends AbstractEntity {

}
