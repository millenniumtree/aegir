<?php

namespace Drupal\aegir_api\Plugin\Field\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;

/**
 * Hard-code entity reference field reference settings.
 */
abstract class AbstractSelection extends DefaultSelection {

  /**
   * The title of the list of bundles to use to create new entities.
   */
  protected $bundleListTitle;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['target_bundles']['#title'] = $this->t($this->bundleListTitle);
    // We only want to allow a single target bundle, so that we can auto-create
    // the proper type of entity. So we switch to 'radios' here (from the
    // default 'checkboxes').
    $form['target_bundles']['#type'] = 'radios';
    $form['target_bundles']['#multiple'] = FALSE;
    // Unwrap the string we need from the array we need to save it as.
    // @see AbstractSelection::elementValidateFilter().
    $form['target_bundles']['#default_value'] = array_pop($form['target_bundles']['#default_value']);
    $form['auto_create']['#default_value'] = FALSE;
    $form['auto_create']['#disabled'] = TRUE;
    $form['sort']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function elementValidateFilter(&$element, FormStateInterface $form_state) {
    // Entity reference fields need 'target_bundles' to be an array. So we wrap
    // the string returned from our radio widget, so that the saved value
    // remains in the same format as with the default checkboxes widget.
    $element['#value'] = [$element['#value'] => $element['#value']];
    $form_state->setValueForElement($element, $element['#value']);
  }

}
