<?php

namespace Drupal\aegir_api\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Overides the 'entity_reference' entity field type.
 */
abstract class AbstractReferenceItem extends EntityReferenceItem {

  /**
   * Title for the field settings form.
   */
  protected $fieldSettingsFormTitle;

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);
    // We only want to allow tasks to be referenced, so lock this down.
    // @TODO Should we hide it too?
    $element['target_type']['#disabled'] = TRUE;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);
    $form['handler']['#title'] = $this->fieldSettingsFormTitle;
    // @TODO Should we also force the 'reference method', or just rely on the default being correct?
    // @TODO Should we also hide this config setting?
    $form['handler']['handler']['#disabled'] = TRUE;
    return $form;
  }

  /**
   * Clean up reference field config settings form.
   */
  public static function fieldConfigEditFormCleanup($plugin_id, &$form) {
    // Check that this is indeed the proper reference form, based on our custom
    // plugin.
    if (!array_key_exists('handler', $form['settings'])) return;
    if (array_key_exists($plugin_id, $form['settings']['handler']['handler']['#options'])) {
      $form['description']['#access'] = FALSE;
      $form['required']['#access'] = FALSE;
      $form['default_value']['#access'] = FALSE;
    }
  }

}
