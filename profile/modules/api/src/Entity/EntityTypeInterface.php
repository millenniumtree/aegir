<?php

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\RevisionableEntityBundleInterface;

/**
 * Provides an interface for defining Ægir entity types.
 */
interface EntityTypeInterface extends RevisionableEntityBundleInterface {

  // Add get/set methods for your configuration properties here.
}
