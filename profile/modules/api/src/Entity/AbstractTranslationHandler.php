<?php

namespace Drupal\aegir_api\Entity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for Ægir entities.
 */
abstract class AbstractTranslationHandler extends ContentTranslationHandler {

}
