<?php

namespace Drupal\aegir_api\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Ægir entities.
 */
abstract class AbstractViewsData extends EntityViewsData implements EntityViewsDataInterface {

}
