<?php

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for Ægir entities.
 *
 * This extends the base storage class, adding required special handling for
 * Ægir entities.
 *
 * @ingroup aegir_api
 */
abstract class AbstractStorage extends SqlContentEntityStorage implements StorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(EntityInterface $entity) {
    $entity_type = $entity->getEntityType();
    $entity_keys = $entity_type->getKeys();
    $revision_ids = $this->database
      ->select($entity_type->getRevisionDataTable(), 'r')
      ->fields('r', [ $entity_keys['revision'] ])
      ->condition('r.' . $entity_keys['id'], $entity->id())
      ->orderBy('r.' . $entity_keys['revision'])
      ->execute()
      ->fetchCol();
    return $revision_ids;
  }

}
