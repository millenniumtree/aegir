<?php

namespace Drupal\aegir_api\Entity\EntityType\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\aegir_api\Logger\AegirLoggerTrait;

/**
 * Builds the form to delete Ægir entity types.
 */
abstract class AbstractDeleteForm extends EntityConfirmFormBase {

  use AegirLoggerTrait;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.' . $this->entity->getEntityTypeId() . '.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();

    $this->log()->success('The @type @label has been deleted.', [
      '@type' => $this->entity->getEntityType()->getSingularLabel(),
      '@label' => $this->entity->label(),
    ]);

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
