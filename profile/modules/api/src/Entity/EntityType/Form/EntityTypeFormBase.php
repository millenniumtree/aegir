<?php

namespace Drupal\aegir_api\Entity\EntityType\Form;

/**
 * Default edit form for Ægir entity types.
 *
 * @package Drupal\aegir_api\Entity\EntityType\Form
 */
class EntityTypeFormBase extends AbstractEntityTypeForm {

}
