<?php

namespace Drupal\aegir_api\Entity\Form;

/**
 * Default form for deleting Ægir entities.
 *
 * @ingroup aegir_api
 */
class DeleteFormBase extends AbstractDeleteForm {

}
