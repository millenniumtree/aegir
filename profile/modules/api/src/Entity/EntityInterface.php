<?php

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Ægir entities.
 *
 * @ingroup aegir_api
 */
interface EntityInterface extends RevisionableInterface, EntityChangedInterface, EntityOwnerInterface {

  /* Add get/set methods for your configuration properties here. */

  /**
   * Gets the Ægir entity type.
   *
   * @return string
   *   The Ægir entity type.
   */
  public function getType();

  /**
   * Gets the Ægir entity name.
   *
   * @return string
   *   Name of the Ægir entity.
   */
  public function getName();

  /**
   * Sets the Ægir entity name.
   *
   * @param string $name
   *   The Ægir entity name.
   *
   * @return \Drupal\aegir_api\Entity\EntityInterface
   *   The called Ægir entity.
   */
  public function setName($name);

  /**
   * Gets the Ægir entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ægir entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Ægir entity creation timestamp.
   *
   * @param int $timestamp
   *   The Ægir entity creation timestamp.
   *
   * @return \Drupal\aegir_api\Entity\EntityInterface
   *   The called Ægir entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Ægir entity published status indicator.
   *
   * Unpublished Ægir entities are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Ægir entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Ægir entity.
   *
   * @param bool $published
   *   TRUE to set this Ægir entity to published, FALSE to set it to
   *   unpublished.
   *
   * @return \Drupal\aegir_api\Entity\EntityInterface
   *   The called Ægir entity.
   */
  public function setPublished($published);

  /**
   * Gets the Ægir entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Ægir entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\aegir_api\Entity\EntityInterface
   *   The called Ægir entity.
   */
  public function setRevisionCreationTime($timestamp);

}
