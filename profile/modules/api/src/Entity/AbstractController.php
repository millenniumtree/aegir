<?php

namespace Drupal\aegir_api\Entity;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AbstractController.
 *
 * Returns responses for Ægir entity routes.
 *
 * @package Drupal\aegir_api\Entity
 */
abstract class AbstractController extends ControllerBase {

  /**
   * An HTML renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * A date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs an AbstractController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   An HTML renderer.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   A date formatter.
   */
  public function __construct(
    RendererInterface $renderer,
    DateFormatterInterface $date_formatter
  ) {
    $this->renderer = $renderer;
    $this->dateFormatter = $date_formatter;

    foreach (['aegirEntityType', 'aegirEntityLabel'] as $property) {
      if (!isset($this->$property)) {
        throw new \LogicException(get_class($this) . " must have a '{$property}' property.");
      }
    }
  }

  /**
   * Return the entity type to which this controller applies.
   */
  protected function getAegirEntityType() {
    return $this->aegirEntityType;
  }

  /**
   * Return the entity label from which construct permissions.
   */
  protected function getAegirEntityLabel() {
    return $this->aegirEntityLabel;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('renderer'),
      $container->get('date.formatter')
    );
  }

  /**
   * Displays a Ægir entity revision.
   *
   * @param int $entity_revision
   *   The Ægir entity revision ID.
   *   *N.B.* The _name_ of this parameter cannot be changed without also
   *   changing calls to (for example) Url::fromRoute in revisionOverview().
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($entity_revision) {
    $entity_type = $this->getAegirEntityType();
    $revision = $this->entityManager()->getStorage($entity_type)->loadRevision($entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder($entity_type);

    return $view_builder->view($revision);
  }

  /**
   * Page title callback for a Ægir Task revision.
   *
   * @param int $entity_revision
   *   The Ægir entity revision ID.
   *   *N.B.* The _name_ of this parameter cannot be changed without also
   *   changing calls to (for example) Url::fromRoute in revisionOverview().
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($entity_revision) {
    $entity_type = $this->getAegirEntityType();
    $entity = $this->entityManager()->getStorage($entity_type)->loadRevision($entity_revision);
    return $this->t('Revision of %title from %date', ['%title' => $entity->label(), '%date' => $this->dateFormatter->format($entity->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of an Ægir entity.
   *
   * *N.B.* This method _must_ be overridden in child entities, so that they
   * can accept parameters _named_ after their entity type.
   *
   * @param \Drupal\aegir_api\Entity\EntityInterface $entity
   *   An Ægir entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   *
   * @todo: Untangle entity routing to clean this up.
   * @see: \Drupal\aegir_task\Entity\Controller::revisionOverview().
   */
  public function revisionOverview(EntityInterface $entity) {
    $account = $this->currentUser();
    $langcode = $entity->language()->getId();
    $langname = $entity->language()->getName();
    $languages = $entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $entity_type = $this->getAegirEntityType();
    $entity_storage = $this->entityTypeManager()->getStorage($entity_type);

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $entity->label()]) : $this->t('Revisions for %title', ['%title' => $entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $entity_label = $this->getAegirEntityLabel();
    $revert_permission = (($account->hasPermission("revert all {$entity_label} revisions") || $account->hasPermission("administer {$entity_label} entities")));
    $delete_permission = (($account->hasPermission("delete all {$entity_label} revisions") || $account->hasPermission("administer {$entity_label} entities")));

    $rows = [];

    $revision_ids = $entity_storage->revisionIds($entity);

    $latest_revision = TRUE;

    foreach (array_reverse($revision_ids) as $revision_id) {
      /** @var \Drupal\aegir_task\Entity\EntityInterface $revision */
      $revision = $entity_storage->loadRevision($revision_id);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($revision_id != $entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.' . $entity_type . '.revision', [
            /*
             * *N.B.* These keys are used as parameter _names_ in this and
             * other methods. As a result, they cannot be changed without
             * updating the method definitions.
             */
            $entity_type => $entity->id(),
            'entity_revision' => $revision_id,
          ], ['attributes' => ['title' => 'view revision']]));
        }
        else {
          $link = $entity->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->revision_log_message->value,
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute($entity_type . '.revision_revert_translation_confirm', [
                $entity_type => $entity->id(),
                'entity_revision' => $revision_id,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute($entity_type . '.revision_revert_confirm', [
                $entity_type => $entity->id(),
                'entity_revision' => $revision_id,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute($entity_type . '.revision_delete_confirm', [
                $entity_type => $entity->id(),
                'entity_revision' => $revision_id,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build[$entity_type . '_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
