<?php

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Class AegirChoiceQuestion.
 *
 * @package Drupal\aegir_api
 */
class AegirChoiceQuestion extends ChoiceQuestion {

  /**
   * The default validator only returns the choice _key_ when the array of
   * choices is associative. Since we use entity IDs as keys, and want to have
   * that ID returned, we need to force the validator to treat all choice
   * arrays as associative.
   */
  protected function isAssoc($array) {
    return TRUE;
  }

}
