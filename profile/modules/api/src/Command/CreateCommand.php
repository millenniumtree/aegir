<?php

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Annotations\DrupalCommand;

/**
 * Class AegirCreateCommand.
 *
 * @package Drupal\aegir_api
 *
 * @DrupalCommand (
 *   extension="aegir_api",
 *   extensionType="module"
 * )
 * @TODO Add tests.
 */
class CreateCommand extends AbstractAegirCommand {

  protected $command_name = 'create';

  protected $options = ['entity_type', 'bundle', 'name'];
  protected $entity_type = FALSE;
  protected $bundle = FALSE;
  protected $name = FALSE;

  protected $confirm = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    if (!$this->confirmed()) return;
    if ($this->createEntity()) {
      $this->io()->success($this->translate('success'));
    }
    else {
      $this->io()->error($this->translate('failure'));
    }
  }

  protected function promptForOption($option) {
    switch ($option) {
      case 'entity_type':
        $choices = $this->getAegirEntityTypeLabels();
        return $this->chooseOption($option, $choices);
      case 'bundle':
        $choices = $this->getAegirBundlesByType($this->entity_type);
        return $this->chooseOption($option, $choices);
      case 'name':
        return $this->askOption($option);
      default:
        # TODO Throw an appropriate exception.
    }
  }

  protected function getReplacement($option) {
    switch ($option) {
      case 'entity_type':
        return $this->getEntityTypeLabel();
      case 'bundle':
        return $this->getBundleLabel();
      case 'name':
        return $this->name;
      default:
        # TODO Throw an appropriate exception.
    }
  }

  protected function getEntityTypeLabel() {
    return $this->getAegirEntityTypeLabels()[$this->entity_type];
  }

  protected function getBundleLabel() {
    return $this->getAegirBundlesByType($this->entity_type)[$this->bundle];
  }

  /**
   * Create an entity.
   */
  protected function createEntity() {
    $type = $this->getAegirEntityTypes()[$this->entity_type];
    $bundle_key = $type->getKey('bundle');
    $label_key = $type->getKey('label');

    return $this
      ->entityTypeManager
      ->getStorage($this->entity_type)
      ->create([
        $bundle_key => $this->bundle,
        $label_key => $this->name,
      ])
      ->save();
  }

}
