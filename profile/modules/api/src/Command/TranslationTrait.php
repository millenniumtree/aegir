<?php

namespace Drupal\aegir_api\Command;

/**
 * Trait TranslationTrait.
 *
 * @package Drupal\aegir_api
 */
trait TranslationTrait {

  protected function translate($key) {
    return strtr(
      $this->trans($this->getCommandKey() . $key),
      $this->getReplacements()
    );
  }

}
