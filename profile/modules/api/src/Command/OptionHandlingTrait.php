<?php

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\Question;
use Drupal\Console\Core\Command\Shared\ContainerAwareCommandTrait;

/**
 * Trait OptionHandlingTrait.
 *
 * @package Drupal\aegir_api
 */
trait OptionHandlingTrait {

  use CommandNameTrait;
  use ConfirmTrait;
  use ContainerAwareCommandTrait;
  use IoTrait;
  use TranslationTrait;

  protected $options = [];

  protected function configureOptions() {
    foreach ($this->options as $option) {
      $this->addOption(
        $option,
        null,
        InputOption::VALUE_REQUIRED,
        $this->translate("options.{$option}.help")
      );
    }
  }

  protected function registerOptions() {
    foreach ($this->options as $option) {
      $this->$option = $this->registerOption($option);
    }
    if ($this->confirm) $this->registerConfirmation();
  }

  protected function determineOptions() {
    foreach ($this->options as $option) {
      $this->$option = $this->determineOption($option);
    }
    if ($this->confirm) $this->determineConfirmation();
  }

  protected function registerOption($option) {
    return $this->$option ?: $this->input()->getOption($option);
  }

  protected function determineOption($option) {
    return $this->$option ?: $this->promptForOption($option);
  }

  protected function chooseOption($option, $choices) {
    $question = new AegirChoiceQuestion(
      $this->translate("options.{$option}.prompt"),
      $choices,
      key($choices)
    );
    $question->setErrorMessage(
      $this->translate("options.{$option}.error")
    );

    return $this->getHelper('question')
      ->ask($this->input(), $this->io(), $question);
  }

  protected function getReplacements() {
    $replacements = [];
    foreach ($this->options as $option) {
      if ($this->$option) {
        $replacements['@' . $option] = $this->getReplacement($option);
      }
    }
    return $replacements;
  }

  protected function askOption($option) {
    $question = new Question(
      $this->translate("options.{$option}.prompt") . ' '
    );

    return $this->getHelper('question')
      ->ask($this->input(), $this->io(), $question);
  }

}
