<?php

namespace Drupal\aegir_api\Command;

use Drupal\Core\Entity\EntityTypeManager;

/**
 * Trait EntityApiTrait.
 *
 * @package Drupal\aegir_api
 */
trait EntityApiTrait {

  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct();
  }

  protected function getAegirBundles() {
    $bundles = [];
    foreach ($this->getAegirEntityTypes() as $type => $entity_type) {
      foreach ($this->getBundleInfo($type) as $bundle => $info) {
        $bundles[$type] = [$bundle => $info['label']];
      }
    }
    return $bundles;
  }

  protected function getAegirBundlesByType($type) {
    return $this->getAegirBundles()[$type];
  }

  protected function getBundleInfo($type) {
    return \Drupal::service('entity_type.bundle.info')->getBundleInfo($type);
  }

  protected function getAegirEntityTypeLabels() {
    $labels = [];
    foreach ($this->getAegirEntityTypes() as $type => $entity_type) {
      # Cast to string.
      $labels[$type] = "{$entity_type->getLabel()}";
    }
    return $labels;
  }

  protected function getAegirEntityTypeLabel($type) {
    return $this->getAegirEntityTypeLabels()[$type];
  }

  protected function getAegirEntityTypes() {
    $types = [];
    foreach ($this->entityTypeManager->getDefinitions() as $type => $entity_type) {
      if ($this->isAnAegirEntityType($type)) {
        $types[$type] = $entity_type;
      }
    }
    return $types;
  }

  protected function isAnAegirEntityType($type) {
    # @TODO Use a class property (or similar) to determine relevant entity
    # types.
    return in_array($type, ['aegir_platform', 'aegir_site']);
  }

  protected function getAegirEntities() {
    $entities = [];
    foreach ($this->getAegirEntityTypes() as $type => $entity_type) {
      $entities[$type] = $this->entityTypeManager->getStorage($type)->loadMultiple();
    }
    return $entities;
  }

  protected function getAegirEntityChoices($entity_type) {
    $options = [];
    foreach ($this->getAegirEntities() as $type => $entities) {
      if ($type !== $entity_type) continue;
      foreach ($entities as $id => $entity) {
        $options[$id] = $entity->getName();
      }
    }
    return $options;
  }



}
