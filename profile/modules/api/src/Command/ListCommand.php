<?php

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Annotations\DrupalCommand;

/**
 * Class ListCommand.
 *
 * @package Drupal\aegir_api
 *
 * @DrupalCommand (
 *   extension="aegir_api",
 *   extensionType="module"
 * )
 * @TODO Add tests.
 */
class ListCommand extends AbstractAegirCommand {

  protected $command_name = 'list';

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $table = new Table($output);
    # TODO Translate these.
    $table->setHeaders(['ID', 'Name', 'Type', 'Bundle']);
    $rows = [];
    foreach ($this->getAegirEntities() as $type => $entities) {
      foreach ($entities as $id => $entity) {
        $rows[] = [
          $id,
          $entity->getName(),
          $entity->getEntityType()->getLabel(),
          $entity->getBundle()->label(),
        ];
      }
    }
    $table
      ->setRows($rows)
      ->render();
  }

}
