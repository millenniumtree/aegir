<?php

namespace Drupal\aegir_operation;

/**
 * A Drupal form wrapper class.
 *
 * @todo: Move to aegir_api abstract/base class, etc.
 */
class Form {

  // The form's definition, passed by reference.
  protected $form = FALSE;

  // The form's state, passed by reference.
  protected $formState = FALSE;

  /**
   * Contructor method.
   *
   * Classes based on HostingForm should be instantiated by passing in $form
   * and $form_state, such as from hook_form_alter(), validate or submit
   * callbacks. These get passed by reference, so that any changes made need
   * not be returned back to the calling function.
   */
  public function __construct(&$form = [], &$form_state = []) {
    $this->setForm($form)->setFormState($form_state);
  }

  /**
   * Set a reference to a form's definition.
   */
  public function setForm(&$form) {
    if (!empty($form)) {
      $this->form =& $form;
    }
    return $this;
  }

  /**
   * Set a reference to a form's state.
   */
  public function setFormState(&$form) {
    if (!empty($form_state)) {
      $this->formState =& $form_state;
    }
    return $this;
  }

  /**
   * Called from implementation of hook_field_widget_form_alter().
   */
  public function hookFieldWidgetFormAlter(&$element, $context) {
    $this->fixInlineEntityForm($element);
  }

  /**
   * Entity types that we edit inline.
   */
  protected $inlineEntityTypes = ['aegir_operation', 'aegir_task'];

  /**
   * Fix how display inline entity forms pointing to Ægir entities.
   */
  protected function fixInlineEntityForm(&$element) {
    if (!array_key_exists('inline_entity_form', $element)) return;
    if (!in_array($element['inline_entity_form']['#entity_type'], $this->inlineEntityTypes)) return;
    /* Remove extra fieldset from inline entity forms. */
    $element['#type'] = 'container';
  }

}
