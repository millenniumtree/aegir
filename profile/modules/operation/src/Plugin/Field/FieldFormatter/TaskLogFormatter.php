<?php

namespace Drupal\aegir_operation\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use SensioLabs\AnsiConverter\AnsiToHtmlConverter;

/**
 * Plugin implementation of the Task Log formatter.
 *
 * @FieldFormatter(
 *   id = "task_log",
 *   label = @Translation("Task Log"),
 *   description = @Translation("Formats task log output with Ansi colours."),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class TaskLogFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [t('Displays task log output.')];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $converter = new AnsiToHtmlConverter();

    $first = 0;
    $last = count($items) - 1;
    $elements = [];
    foreach ($items as $delta => $item) {
      switch ($delta) {
        case $first:
          $class = 'task-log-row first-task-log-row';
          break;
        case $last:
          $class = 'task-log-row last-task-log-row';
          break;
        default:
          $class = 'task-log-row';
      }
      // Render each element as markup.
      $elements[$delta] = [
        '#type' => 'markup',
        // @todo Pass through theme function, filter for XSS, style via CSS, etc.
        '#children' => '<pre class="' . $class . '">' . $converter->convert($item->value) . '</pre>',
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $elements = parent::view($items, $langcode);
    $elements['#prefix'] = '<div class="task-log-field">';
    $elements['#suffix'] = '</div>';
    $elements['#attached']['library'][] = 'aegir_operation/task_log';
    return $elements;
  }

}
