<?php

namespace Drupal\aegir_operation\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the Task Log formatter.
 *
 * @FieldFormatter(
 *   id = "operation_status_icon",
 *   label = @Translation("Operation status icon"),
 *   description = @Translation("Formats an operation status as an appropriate icon."),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class OperationStatusIconFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [t('Displays operation status as an icon.')];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $elements[$delta] = [
        '#type' => 'markup',
        // @todo Pass through theme function, filter for XSS, style via CSS, etc.
        '#children' => $this->getStatusIcon($item->value),
      ];
    }

    return $elements;
  }

  /**
   * Return an icon to represent the operation status.
   *
   * @TODO Pass this through a theme function, to allow icon overrides.
   */
  protected function getStatusIcon($status) {
    switch ($status) {
      case 'changed':
      case 'ok':
        $icon = '<i class="fa fa-check-circle"></i>';
        break;
      case 'dispatched':
        $icon = '<div class="three-cogs fa-1x">';
        $icon .= '<i class="fa fa-cog fa-spin fa-2x fa-fw"></i>';
        $icon .= '<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>';
        $icon .= '<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>';
        $icon .= '</div>';
        break;
      case 'warnings':
        $icon = '<i class="fa fa-exclamation-circle"></i>';
        break;
      case 'failed':
        $icon = '<i class="fa fa-exclamation-triangle"></i>';
        break;
      case 'unreachable':
        $icon = '<i class="fa fa-unlink"></i>';
        break;
      case 'none':
        $icon = '<i class="fa fa-caret-square-o-right"></i>';
        break;
      case 'unknown':
      default:
        $icon = '<i class="fa fa-question-circle"></i>';
    }
    return '<div class="operation-status-icon operation-status-' . $status . '">' . $icon . '</div>';
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $elements = parent::view($items, $langcode);
    $elements['#attached']['library'][] = 'aegir_operation/operation_views';
    return $elements;
  }

}
