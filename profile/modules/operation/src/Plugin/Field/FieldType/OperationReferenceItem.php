<?php

namespace Drupal\aegir_operation\Plugin\Field\FieldType;

use Drupal\aegir_api\Plugin\Field\FieldType\AbstractReferenceItem;

/**
 * Defines the 'operation_reference' entity field type.
 *
 * Supported settings (below the definition's 'settings' key) are:
 * - target_type: The entity type to reference. Required.
 *
 * @FieldType(
 *   id = "operation_reference",
 *   label = @Translation("Operation"),
 *   description = @Translation("An entity field containing a operation entity reference."),
 *   category = @Translation("Reference"),
 *   default_widget = "aegir_operation_autocreate",
 *   default_formatter = "aegir_reference_entity_view",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   cardinality = 1,
 * )
 * @TODO We enforce cardinality here, because we're currently adding each
 * operation by adding a separate field. It might be preferable to have single
 * "operations" field to contain all of an entity's operations. This single
 * field could then be added as a base field to the entity type. This could
 * approach could also simplify views relationships, since we could use a
 * single "operations" field to determine thos attached to a given entity.
 * @TODO We need to provide new widgets and formatters, since these only work
 * with entity_reference fields. We might be able to alter that behaviour in a
 * hook, but we're going to want to alter the auto_create behaviour, which I
 * believe is handled by the widget.
 */
class OperationReferenceItem extends AbstractReferenceItem {

  /**
   * {@inheritdoc}
   */
  protected $fieldSettingsFormTitle = 'Operation reference type';

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return ['target_type' => 'aegir_operation'];
  }


}
