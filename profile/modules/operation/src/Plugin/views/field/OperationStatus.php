<?php

namespace Drupal\aegir_operation\Plugin\views\field;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Render\ViewsRenderPipelineMarkup;
use Drupal\views\ResultRow;

/**
 * Field handler to display an operations status.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("operation_status")
 */
class OperationStatus extends FieldPluginBase {

  /**
   * {@inheritdoc}
   *
   * @TODO Determine whether this is needed.
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // do nothing -- to override the parent query.
    // @TODO Look up the operation status field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Override the alter text option to always alter the text.
    $options['alter']['contains']['alter_text'] = array('default' => TRUE);
    $options['hide_alter_empty'] = array('default' => FALSE);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // @TODO Provide an option to display text or an icon.

    // Remove the checkbox
    unset($form['alter']['alter_text']);
    unset($form['alter']['text']['#states']);
    unset($form['alter']['help']['#states']);
  }

  /**
   * Return an icon to represent the operation status.
   *
   * @TODO Pass this through a theme function, to allow icon overrides.
   */
  protected function getStatusIcon($status) {
    switch ($status) {
      case 'changed':
      case 'ok':
        $icon = '<i class="fa fa-check-circle"></i>';
        break;
      case 'dispatched':
        $icon = '<div class="three-cogs fa-1x">';
        $icon .= '<i class="fa fa-cog fa-spin fa-2x fa-fw"></i>';
        $icon .= '<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>';
        $icon .= '<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>';
        $icon .= '</div>';
        break;
      case 'warnings':
        $icon = '<i class="fa fa-exclamation-circle"></i>';
        break;
      case 'error':
      case 'failed':
        $icon = '<i class="fa fa-exclamation-triangle"></i>';
        break;
      case 'unknown':
        $icon = '<i class="fa fa-question-circle"></i>';
        break;
      case 'unreachable':
        $icon = '<i class="fa fa-unlink"></i>';
        break;
      case 'none':
        $icon = '<i class="fa fa-play-circle"></i>';
        break;
      default:
        $icon = '<i class="fa fa-question-circle"></i>';
    }
    return '<div class="operation-status-icon operation-status-' . $status . '">' . $icon . '</div>';
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {
    $status = $this->getEntity($row)->getStatus();
    $this->options['alter']['text'] = $this->getStatusIcon($status);

    // Return some text, so the code never thinks the value is empty.
    return ViewsRenderPipelineMarkup::create(Xss::filterAdmin($this->options['alter']['text']));
  }

}
