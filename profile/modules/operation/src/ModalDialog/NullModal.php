<?php

namespace Drupal\aegir_operation\ModalDialog;

/**
 * A concrete (non-abstract) Modal class.
 *
 * This allows instantiation in routes (e.g. modal close).
 */
class NullModal extends AbstractModal {

  /**
   * {@inheritdoc}
   */
  protected function getContent() {}

}
