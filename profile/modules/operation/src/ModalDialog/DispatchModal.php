<?php

namespace Drupal\aegir_operation\ModalDialog;

use Drupal\aegir_operation\Operation;
use Drupal\aegir_operation\Entity\Entity as OperationEntity;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Modal dialog to display an operation entity form, if required, before
 * dispatching it to the queue.
 */
class DispatchModal extends AbstractModal {

  /**
   * {@inheritdoc}
   */
  protected $title = 'Run operation';

  /**
   * {@inheritdoc}
   *
   */
  public function openModal(OperationEntity $operation) {
    $this->setEntity($operation);
    if ($this->dispatchRequiresInput()) {
      return parent::openModal($operation);
    }
    return $this->dispatchOperation();
  }

  /**
   * Determine whether a given operation has fields.
   */
  protected function dispatchRequiresInput() {
    return (boolean) count($this->getEntity()->marshal());
  }

  /**
   * {@inheritdoc}
   *
   * Provides the operation dispatch form.
   *
   * @see: operation/src/Entity/Entity.php
   * @see: operation/src/Entity/Form/DispatchForm.php
   */
  protected function getContent() {
    return \Drupal::service('entity.form_builder')
      ->getForm($this->getEntity(), 'dispatch');
  }

  /**
   * Dispatch an operation to the queue.
   */
  protected function dispatchOperation() {
    $operation = new Operation($this->getEntity());
    $operation->dispatch();

    // @TODO Add some error checking/logging?
    $referer = \Drupal::request()->headers->get('referer');
    $response = new RedirectResponse($referer);
    return $response;
  }

}
