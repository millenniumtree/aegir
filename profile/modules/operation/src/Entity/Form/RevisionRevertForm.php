<?php

namespace Drupal\aegir_operation\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionRevertForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Ægir Operation revision.
 *
 * @ingroup aegir_operation
 */
class RevisionRevertForm extends AbstractRevisionRevertForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.field_manager')->getStorage('aegir_operation'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_operation_revision_revert_confirm';
  }

}
