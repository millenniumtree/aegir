<?php

namespace Drupal\aegir_operation\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionRevertTranslationForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Ægir operation revision for a translation.
 *
 * @ingroup aegir_operation
 */
class RevisionRevertTranslationForm extends AbstractRevisionRevertTranslationForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.field_manager')->getStorage('aegir_operation'),
      $container->get('date.formatter'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_operation_revision_revert_translation_confirm';
  }

}
