<?php

namespace Drupal\aegir_operation\Entity\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\aegir_operation\Entity\Entity as OperationEntity;
use Symfony\Component\Routing\Route;

class UuidParamConverter implements ParamConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    return $this->getOperationEntity($value);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return (!empty($definition['type']) && $definition['type'] == 'operation_uuid');
  }

  /**
   * Return the operation entity from its UUID.
   */
  public function getOperationEntity(string $uuid) {
    $entity = \Drupal::service('entity.repository')->loadEntityByUuid('aegir_operation', $uuid);
    // @TODO Switch to checking for an interface.
    if ($entity instanceof OperationEntity) {
      return $entity;
    }
    $this->log->error($this->t('Failed to load operation entity from UUID: :uuid.', [':uuid' => $uuid]));
    return FALSE;
  }

}
