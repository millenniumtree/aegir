<?php

/**
 * @file
 * Contains aegir_task.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\aegir_api\Entity\EntityInterface;
use Drupal\aegir_task\Plugin\Field\FieldType\TaskReferenceItem;

/**
 * Implements hook_help().
 */
function aegir_task_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the aegir_task module.
    case 'help.page.aegir_task':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('A task definition used to compose an operation.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function aegir_task_theme() {
  $theme = [];
  $theme['aegir_task'] = [
    'render element' => 'elements',
    'file' => 'aegir_task.page.inc',
    'template' => 'aegir_task',
  ];
  $theme['aegir_task_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'aegir_task.page.inc',
  ];
  return $theme;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function aegir_task_theme_suggestions_aegir_task(array $variables) {
  $suggestions = [];
  $entity = $variables['elements']['#aegir_task'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'aegir_task__' . $sanitized_view_mode;
  $suggestions[] = 'aegir_task__' . $entity->bundle();
  $suggestions[] = 'aegir_task__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'aegir_task__' . $entity->id();
  $suggestions[] = 'aegir_task__' . $entity->id() . '__' . $sanitized_view_mode;
  return $suggestions;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function aegir_task_form_field_config_edit_form_alter(&$form, &$form_state, $form_id) {
  TaskReferenceItem::fieldConfigEditFormCleanup('default:aegir_task', $form);
}
