<?php

namespace Drupal\aegir_task\Entity\Form;

use Drupal\inline_entity_form\Form\EntityInlineForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Task inline form handler.
 */
class TaskInlineForm extends EntityInlineForm {

  protected $entityForm = NULL;
  protected $formState = NULL;

  protected $taskDependencies = NULL;
  protected $dependency = NULL;
  protected $dependencyId = NULL;
  protected $dependencyDefaults = [
    'hide_dependent' => TRUE,
    'use_ajax' => TRUE,
  ];

  /**
   * {@inheritdoc}
   */
  public function entityForm(array $entity_form, FormStateInterface $form_state) {
    // @TODO Could initial form validation errors be triggered in the parent form build?
    $entity_form = parent::entityForm($entity_form, $form_state);
    $this->entityForm = $entity_form;
    $this->formState = $form_state;
    $this->processTaskDependencies();
    return $this->entityForm;
  }

  /**
   * Process any task dependencies for the current bundle.
   */
  protected function processTaskDependencies() {
    foreach ($this->getTaskDependencies() as $id => $dependency) {
      if ($this->entityForm['#bundle'] == $dependency['dependent']['bundle']) {
        $this->setDependency($dependency, $id);
        $this->hideDependentField();
        $this->addDependentFieldWrapper();

	// Invoke the dependency callback, passing it the dependent field (by
	// reference, so it can be altered) along with the value of the
	// dependee field.
        $dependent_field =& $this->entityForm[$dependency['dependent']['field']];
        $dependee_value = $this->getDependeeValue();
        call_user_func_array($dependency['callback'], [&$dependent_field, $dependee_value]);
        $this->setDependentFieldValue($dependent_field);
        $this->fixDependentFieldValidation();
      }
      if ($this->entityForm['#bundle'] == $dependency['dependee']['bundle']) {
        $this->setDependency($dependency, $id);
        $this->addDependeeAjax();
      }
    }
  }

  /**
   * Set the proper value for the dependent field.
   */
  protected function setDependentFieldValue(&$field) {
    $this->setDependentFieldDefault($field);

    $dependent_bundle = $this->dependency['dependent']['bundle'];
    $current_value = $this->getDependentValue($dependent_bundle);
    if (!is_null($current_value)) {
      $field['widget']['#default_value'] = $current_value;
    }
  }

  /**
   * Set a default value for the dependent field.
   */
  protected function setDependentFieldDefault(&$field) {
    $current_default = $field['widget']['#default_value'];
    $options = $field['widget']['#options'];
    if (!array_key_exists($current_default, $options)) {
      reset($options);
      $new_default = key($options);
      $field['widget']['#default_value'] = $new_default;
      unset($field['widget']['#value']);
    }
  }

  /**
   * Allow dependent field validation to pass.
   */
  protected function fixDependentFieldValidation() {
    // Overridden option values will fail validation.
    // @TODO Handle validation properly.
    unset($this->entityForm['#element_validate']);
  }

  /**
   * Fill in the current dependency with default and store it as an object property.
   */
  protected function setDependency($dependency, $id) {
    $this->dependency = $dependency + $this->dependencyDefaults;
    $this->dependencyId = $id;
  }

  /**
   * Invoke hooks to build task dependencies.
   */
  protected function getTaskDependencies() {
    if (is_null($this->taskDependencies)) {
      $module_handler = \Drupal::moduleHandler();
      $task_dependencies = $module_handler->invokeAll('aegir_task_dependencies');
      $module_handler->alter('aegir_task_dependencies', $task_dependencies);
      $this->taskDependencies = $task_dependencies;
    }
    return $this->taskDependencies;
  }

  /**
   * Hide the dependent field when the dependee field does not yet have a value.
   */
  protected function hideDependentField() {
    if (!$this->dependency['hide_dependent']) return;

    $dependee_bundle = $this->dependency['dependee']['bundle'];
    $dependee_task_field_name = $this->getTaskFieldName($dependee_bundle);
    $dependee_field = $this->dependency['dependee']['field'];

    $selector = ':input[name="';
    $selector .= $dependee_task_field_name;
    $selector .= '[0][inline_entity_form][';
    $selector .= $dependee_field;
    $selector .= ']"]';

    $dependent_field = $this->dependency['dependent']['field'];
    $this->entityForm[$dependent_field]['#states']['invisible'] = [
      $selector => ['value' => '_none'],
    ];
  }

  /**
   * Add wrapper to allow AJAX replacement of dependent field.
   */
  protected function addDependentFieldWrapper() {
    $dependent_field = $this->dependency['dependent']['field'];
    $wrapper_id = $this->getDependentFieldWrapperId();
    $this->entityForm[$dependent_field]['#prefix'] = "<div id='{$wrapper_id}'>";
    $this->entityForm[$dependent_field]['#suffix'] = '</div>';
  }

  /**
   * Add AJAX to replace the dependent field whenever the dependee changes.
   */
  protected function addDependeeAjax() {
    if (!$this->dependency['use_ajax']) return;

    $dependee_field = $this->dependency['dependee']['field'];
    $this->entityForm[$dependee_field]['widget']['#ajax'] = [
      'callback' => [$this, 'ajaxCallback'],
      'wrapper' => $this->getDependentFieldWrapperId(),
      // @TODO Remove progress?
      'progress' => [
        'type' => 'throbber',
        'message' => t('Updating %field field...', [
          '%field' => $this->getDependentFieldLabel(),
        ]),
      ],
    ];

    $dependent_bundle = $this->dependency['dependent']['bundle'];
    $this->entityForm[$dependee_field]['widget']['#task_dependency'] = [
      'dependent_field' => $this->dependency['dependent']['field'],
      'dependent_task_field_name' => $this->getTaskFieldName($dependent_bundle),
    ];

    $this->entityForm[$dependee_field]['#task_dependency'] = $this->dependency;
  }

  /**
   * An AJAX callback to return the relevant rebuilt form element.
   */
  public static function ajaxCallback(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $dependent_field = $element['#task_dependency']['dependent_field'];
    $dependent_task_field_name = $element['#task_dependency']['dependent_task_field_name'];

    return $form[$dependent_task_field_name]['widget']['0']['inline_entity_form'][$dependent_field];
  }

  /**
   * Return the wrapper to use when targetting the dependent field.
   */
  protected function getDependentFieldWrapperId() {
    // @TODO Ensure the ID is valid.
    return str_replace(':', '-', $this->dependencyId);
  }

  /**
   * Return the label of the dependent field.
   */
  protected function getDependentFieldLabel() {
    $dependent_field = $this->getDependentField();
    if (is_null($dependent_field)) return;
    return $dependent_field->getFieldDefinition()->getLabel();
  }

  /**
   * Return the dependent field.
   */
  protected function getDependentField() {
    $dependent_bundle = $this->dependency['dependent']['bundle'];
    $task_field = $this->getTaskFieldName($dependent_bundle);
    $dependent_field = $this->dependency['dependent']['field'];
    $task = $this->getTaskFromFieldName($task_field);
    if (is_null($task)) return;
    return $task->get($dependent_field);
  }

  /**
   * Return the relevant dependee task's field value.
   */
  protected function getDependeeValue() {
    $dependee_bundle = $this->dependency['dependee']['bundle'];
    $task_field = $this->getTaskFieldName($dependee_bundle);
    $dependee_field = $this->dependency['dependee']['field'];

    if ($this->isRebuiltForm() || $this->isSubmittedForm()) {
      // Form has been submitted/rebuilt. Use the current form_state input.
      $input = $this->formState->getUserInput();
      return $input[$task_field][0]['inline_entity_form'][$dependee_field];
    }
    else {
      // Form was just loaded/built. Use the entity default value.
      $task = $this->getTaskFromFieldName($task_field);
      if (is_null($task)) return '';
      $task_value = $task->get($dependee_field)->getValue();
      if (empty($task_value)) return '';
      return $task_value[0]['target_id'];
    }
  }

  /**
   * Determine whether the current form was rebuilt or initially loaded.
   */
  protected function isRebuiltForm() {
    $input = $this->formState->getUserInput();
    return array_key_exists('_triggering_element_name', $input);
  }

  /**
   * Determine whether the current form is being submitted.
   */
  protected function isSubmittedForm() {
    $dependee_bundle = $this->dependency['dependee']['bundle'];
    $task_field = $this->getTaskFieldName($dependee_bundle);
    $input = $this->formState->getUserInput();
    return !$this->isRebuiltForm() && array_key_exists($task_field, $input);
  }

  /**
   * Return the relevant dependent task's field value.
   */
  protected function getDependentValue() {
    $dependent_bundle = $this->dependency['dependent']['bundle'];
    $task_field = $this->getTaskFieldName($dependent_bundle);
    $dependent_field = $this->dependency['dependent']['field'];
    $task = $this->getTaskFromFieldName($task_field);
    if (is_null($task)) return;
    $task_value = $task->get($dependent_field)->getValue();
    if (empty($task_value)) return;
    return $task_value[0]['value'];
  }

  /**
   * Given an operation task field name, return the task entity.
   */
  protected function getTaskFromFieldName($task_field_name) {
    $complete_form = $this->formState->getCompleteForm();
    $inline_entity_form = $complete_form[$task_field_name]['widget'][0]['inline_entity_form'];
    if (isset($inline_entity_form['#default_value'])) {
      return $inline_entity_form['#default_value'];
    }
    return isset($inline_entity_form['#entity']) ? $inline_entity_form['#entity'] : NULL;
  }

  /**
   * Return the name of the operation entity's field referencing the task entity bundle.
   */
  protected function getTaskFieldName($bundle) {
    foreach ($this->formState->getCompleteForm() as $name => $field) {
      # Filter non-field form array elements.
      if (!isset($field['widget']['#field_name'])) continue;
      if ($field['widget'][0]['inline_entity_form']['#bundle'] == $bundle) {
        return $name;
      }
    }
  }

}
