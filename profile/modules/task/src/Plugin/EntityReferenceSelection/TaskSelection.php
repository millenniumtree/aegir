<?php

namespace Drupal\aegir_task\Plugin\EntityReferenceSelection;

use Drupal\aegir_api\Plugin\Field\EntityReferenceSelection\AbstractSelection;

/**
 * Provides specific access control for the user entity type.
 *
 * @EntityReferenceSelection(
 *   id = "default:aegir_task",
 *   label = @Translation("Task selection"),
 *   entity_types = {"aegir_task"},
 *   group = "default",
 *   weight = 0
 * )
 */
class TaskSelection extends AbstractSelection {

  /**
   * {@inheritdoc}
   */
  protected $bundleListTitle = 'Task type';

}
