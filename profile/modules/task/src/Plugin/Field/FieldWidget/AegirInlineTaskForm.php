<?php

namespace Drupal\aegir_task\Plugin\Field\FieldWidget;

use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormSimple;

/**
 * Simple inline widget that applies to task reference fields.
 *
 * @FieldWidget(
 *   id = "aegir_inline_task_form",
 *   label = @Translation("Inline task form"),
 *   field_types = {
 *     "task_reference",
 *   },
 *   multiple_values = false
 * )
 */
class AegirInlineTaskForm extends InlineEntityFormSimple {

}
