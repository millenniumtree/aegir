<?php

namespace Drupal\aegir_task\Plugin\Field\FieldType;

use Drupal\aegir_api\Plugin\Field\FieldType\AbstractReferenceItem;

/**
 * Defines the 'task_reference' entity field type.
 *
 * Supported settings (below the definition's 'settings' key) are:
 * - target_type: The entity type to reference. Required.
 *
 * @FieldType(
 *   id = "task_reference",
 *   label = @Translation("Task"),
 *   description = @Translation("An entity field containing a task entity reference."),
 *   category = @Translation("Reference"),
 *   default_widget = "aegir_inline_task_form",
 *   default_formatter = "aegir_reference_entity_view",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   cardinality = 1,
 * )
 * @TODO See OperationReferenceItem for todos related to our custom field types.
 */
class TaskReferenceItem extends AbstractReferenceItem {

  /**
   * {@inheritdoc}
   */
  protected $fieldSettingsFormTitle = 'Task reference type';

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return ['target_type' => 'aegir_task'];
  }

}
