# TODO: re-enable and fix task manager permissions
@tasks @entities @api @disabled
Feature: Create and manage Aegir tasks
  In order to define tasks that can be combined into applications,
  as a task manager,
  I want to be able to manage Aegir task entities.

  Background:
    Given I run "drush -y en aegir_test_task"
      And I am logged in as a "Task manager"
      And I am on "admin/aegir/tasks"

  Scenario: Create tasks
     When I click "Add Ægir task"
     Then I should be on "admin/aegir/tasks/add"
      And I should see the heading "Add task" in the "header" region
      And I should see the link "TEST_TASK_TYPE_1"
      And I should see the link "TEST_TASK_TYPE_2"
     When I click "TEST_TASK_TYPE_1" in the "content" region
     Then I should be on "admin/aegir/task/add/test_task_type_1"
     When I fill in "Name" with "TEST_TASK_A"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_TASK_A task."
      And I should see the link "View" in the "tabs" region
      And I should see the link "Edit" in the "tabs" region
      And I should see the link "Revisions" in the "tabs" region
      And I should see the link "Delete" in the "tabs" region


  Scenario: Update tasks
    Given I should see the link "TEST_TASK_A" in the "content" region
      And I click "TEST_TASK_A"
     When I check the box "Create new revision"
      And for "Revision log message" I enter "Test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_TASK_A task."

  Scenario: Delete tasks
    Given I click "TEST_TASK_A" in the "content" region
     Then I should see the heading "Edit TEST_TASK_A" in the "header" region
     When I click "Delete" in the "content" region
     Then I should see the heading "Are you sure you want to delete the task TEST_TASK_A?" in the "header" region
     When I press the "Delete" button
     Then I should see the success message "The task TEST_TASK_A has been deleted."
      And I should be on "admin/aegir/tasks"
      And I should not see the link "TEST_TASK_A"

