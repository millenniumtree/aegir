<?php

namespace Drupal\aegir_site\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionDeleteForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Ægir site revision.
 *
 * @ingroup aegir_site
 */
class RevisionDeleteForm extends AbstractRevisionDeleteForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.field_manager');
    return new static(
      $entity_manager->getStorage('aegir_site'),
      $container->get('database')
    );
  }

}
