<?php

namespace Drupal\aegir_site\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionRevertTranslationForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Ægir site revision for a translation.
 *
 * @ingroup aegir_site
 */
class RevisionRevertTranslationForm extends AbstractRevisionRevertTranslationForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.field_manager')->getStorage('aegir_site'),
      $container->get('date.formatter'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_site_revision_revert_translation_confirm';
  }

}
