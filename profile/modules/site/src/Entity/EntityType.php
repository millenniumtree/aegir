<?php

namespace Drupal\aegir_site\Entity;

use Drupal\aegir_api\Entity\AbstractEntityType;

/**
 * Defines an Ægir site type entity.
 *
 * @ConfigEntityType(
 *   id = "aegir_site_type",
 *   label = @Translation("Site type"),
 *   handlers = {
 *     "list_builder" = "Drupal\aegir_api\Entity\EntityType\ListBuilderBase",
 *     "form" = {
 *       "add" = "Drupal\aegir_api\Entity\EntityType\Form\EntityTypeFormBase",
 *       "edit" = "Drupal\aegir_api\Entity\EntityType\Form\EntityTypeFormBase",
 *       "delete" = "Drupal\aegir_api\Entity\EntityType\Form\DeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_api\Entity\EntityType\HtmlRouteProviderBase",
 *     },
 *   },
 *   config_prefix = "aegir_site_type",
 *   admin_permission = "administer aegir site types",
 *   bundle_of = "aegir_site",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/sites/types/{aegir_site_type}",
 *     "add-form" = "/admin/aegir/sites/types/add",
 *     "edit-form" = "/admin/aegir/sites/types/{aegir_site_type}/edit",
 *     "delete-form" = "/admin/aegir/sites/types/{aegir_site_type}/delete",
 *     "collection" = "/admin/aegir/sites/types"
 *   }
 * )
 */
class EntityType extends AbstractEntityType {

}
