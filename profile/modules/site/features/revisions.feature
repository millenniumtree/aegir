@sites @revisions @api @wip @disabled
Feature: Create and manage Aegir sites
  In order to define sites that can be combined into applications,
  as a site manager,
  I want to be able to manage Aegir site entities.

  Background:
    Given I run "drush -y en aegir_test_site"
      And I am logged in as a "Site manager"
      And I am on "admin/aegir/sites"

  Scenario: Create a site for later scenarios
    Given I click "Add Ægir site"
      And I click "TEST_SITE_TYPE_1" in the "content" region
      And I fill in "Name" with "TEST_SITE_B"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_SITE_B site."
      And I should see the link "Revisions" in the "tabs" region

  Scenario: Update the site to create some revisions
    Given I should see the link "TEST_SITE_B" in the "content" region
      And I click "TEST_SITE_B"
     When I fill in "Name" with "TEST_SITE_B-changed"
      And I check the box "Create new revision"
      And for "Revision log message" I enter "First test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_SITE_B-changed site."
     When I click "Edit" in the "tabs" region
      And I fill in "Name" with "TEST_SITE_B"
      And I check the box "Create new revision"
      And for "Revision log message" I enter "Second test log message."
      And I press the "Save" button
      And I click "Revisions"
     Then I should see the heading "Revisions for TEST_SITE_B" in the "header" region
      And I should see the text "First test log message."
      And I should see the text "Second test log message."

  Scenario: View a site revision
    Given I click "TEST_SITE_B"
      And I click "Revisions"
     When I click "view revision" in the "First test log message." row
     Then I should see "Revision of TEST_SITE_B-changed from" in the "header" region

  Scenario: Revert a site revision
    Given I click "TEST_SITE_B"
      And I click "Revisions"
     When I click "Revert" in the "First test log message." row
     Then I should see the text "Are you sure you want to revert to the revision from" in the "header" region
     When I press "Revert"
     #Then I should see the success message "The site TEST_SITE_B has been reverted to the revision from"


