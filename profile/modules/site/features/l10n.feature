@sites @l10n @api @wip @disabled
Feature: Translate Aegir sites
  In order to allow usage in multiple languages,
  as a site manager,
  I want to be able to translate Aegir site entities.

  Background:
    Given I am logged in as a "Site manager"

  Scenario: Enable a new language (French)
    Given I run "drush -y en aegir_test_site"
      And I am logged in as an "Administrator"
      And I am on "admin/config/regional/language"
     When I click "Add language"
     Then I should see the heading "Add language"
     When I select "French" from "Language name"
      And I press the "Add language" button
     Then I should see the success message "The language French has been created and can now be used."
      And I am on "admin/config/regional/content-language"
     When I check the box "Site"
      And I check the box "settings[aegir_site][test_site_type_1][translatable]"
      And I check the box "settings[aegir_site][test_site_type_1][fields][user_id]"
      And I check the box "settings[aegir_site][test_site_type_1][settings][language][language_alterable]"
      And I press the "Save configuration" button
     Then I should see the success message "Settings successfully updated."

  Scenario: Create English site
    Given I am on "admin/aegir/site/add/test_site_type_1"
     When I fill in "Name" with "TEST_SITE_ENG"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_SITE_ENG site."

  Scenario: Translate site
    Given I am on "admin/aegir/sites"
     Then I should see the link "TEST_SITE_ENG" in the "content" region
     When I click "TEST_SITE_ENG"
     Then I should see the link "Translate" in the "tabs" region
     When I click "Translate"
      And I click "Add" in the "French" row
     Then I should see the heading "Create French translation of TEST_SITE_ENG"
     When I fill in "Name" with "TEST_SITE_FR"
      And I check the box "Create new revision"
      And for "Revision log message" I enter "Première message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_SITE_FR site."
      And the url should match "fr/admin/aegir/sites/[0-9]+?"

  Scenario: Update the translated site to create some revisions
    Given I am on "fr/admin/aegir/sites"
     When I click "TEST_SITE_FR"
     Then the url should match "fr/admin/aegir/sites/[0-9]+?"
     When I click "Edit" in the "tabs" region
     Then I should see the heading "TEST_SITE_FR [French translation]" in the "header" region
      And the "Create new revision" checkbox should not be checked
     When I check the box "Create new revision"
      And for "Revision log message" I enter "Deuxième message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_SITE_FR site."
      And the url should match "fr/admin/aegir/sites/[0-9]+?"

  Scenario: Revert a translated site revision
    Given I am on "fr/admin/aegir/sites"
     When I click "TEST_SITE_FR"
      And I click "Revisions"
     Then I should see the text "Première message." in the "content" region
      And I should see the text "Deuxième message." in the "content" region
     When I click "Revert" in the "Première message." row
     Then I should see the text "Are you sure you want to revert the French translation to the revision from" in the "header" region
     When I press "Revert"
     Then I should see the success message "The site TEST_SITE_FR has been reverted to the revision from"

  Scenario: Delete site translation
    Given I am on "admin/aegir/sites"
     When I click "TEST_SITE_FR"
      And I click "Translate"
      And I click "Delete" in the "French" row
     Then I should see the heading "Are you sure you want to delete the French translation of the site TEST_SITE_FR?"
     When I press "Delete French translation"
     Then I should see the success message "The site TEST_SITE_FR French translation has been deleted."

