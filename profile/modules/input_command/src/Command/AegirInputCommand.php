<?php

namespace Drupal\aegir_input_command\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Core\Entity\EntityRepository;

/**
 * Class AegirInputCommand.
 *
 * @package Drupal\aegir_input_command
 *
 * DrupalCommand (
 *   extension="aegir_input_command",
 *   extensionType="module"
 * )
 */
class AegirInputCommand extends ContainerAwareCommand {

  # @TODO: Fix translations. See:https://github.com/hechoendrupal/drupal-console/issues/3010

  /**
   * The EntityRepository service.
   *
   * @var EntityRepository
   */
  protected $entityRepository;

  /**
   * Constructs a new AegirInputCommand object.
   */
  public function __construct(EntityRepository $entity_repository) {
    $this->entityRepository = $entity_repository;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('aegir:input')
      #->setDescription($this->trans('commands.aegir.input.description'))
      ->setDescription('Update values in the Aegir front-end.')
      ->setHidden(true)
      ->addArgument(
        'type',
        InputArgument::REQUIRED,
        #$this->trans('commands.aegir.input.arguments.type')
        $this->trans('The type of entity into which we will be inputting data.')
      )
      ->addArgument(
        'uuid',
        InputArgument::REQUIRED,
        #$this->trans('commands.aegir.input.arguments.uuid')
        $this->trans('The UUID of the entity into which we will be inputting data.')
      )
      ->addArgument(
        'field',
        InputArgument::REQUIRED,
        #$this->trans('commands.aegir.input.arguments.field')
        $this->trans('The field into which we will be inputting data.')
      )
      # @TODO: Validate that 'data' is a string. For now.
      ->addArgument(
        'data',
        InputArgument::REQUIRED,
        #$this->trans('commands.aegir.input.arguments.data')
        $this->trans('The data to input into a front-end field.')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);
    $args = $input->getArguments();
    foreach ($args as $key => $value) {
      $io->info($key . ': ' . $value);
    }

    $entity = $this->entityRepository->loadEntityByUuid($args['type'], $args['uuid']);

    if (is_null($entity)) return $io->error(sprintf(
      #$this->trans('commands.aegir.input.errors.invalid-uuid'),
      $this->trans('No %s found with UUID %s'),
      $args['type'],
      $args['uuid']
    ));

    if (!$entity->hasField($args['field'])) return $io->error(sprintf(
      #$this->trans('commands.aegir.input.errors.invalid-field'),
      $this->trans('No %s field found on %s entity type.'),
      $args['field'],
      $args['type']
    ));

    $new_value = $this->getNewValue($entity, $args);
    $entity->set($args['field'], $new_value);

    // @TODO: Figure out how to properly handle revisions.
    if (method_exists($entity, 'createNewRevision')) {
      $entity->createNewRevision(FALSE);
    }

    $entity->save();
  }

  /**
   * Return the new field value.
   */
  protected function getNewValue($entity, $args) {
    # @TODO: Consider how to handle limited multi-value fields.
    # @TODO: Consider how to handle non-text fields (ie. entity references)
    $field = $entity->get($args['field']);
    if (!$this->shouldAppend($field)) return $args['data'];

    $field->appendItem($args['data']);
    return $field->getValue();
  }

  /**
   * Determine whether to append to or overwrite the field.
   */
  protected function shouldAppend($field) {
    $cardinality = $field
      ->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();
    return $cardinality != 1;
  }

}
