<?php

namespace Drupal\aegir_platform\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionRevertForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Ægir Platform revision.
 *
 * @ingroup aegir_platform
 */
class RevisionRevertForm extends AbstractRevisionRevertForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.field_manager')->getStorage('aegir_platform'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_platform_revision_revert_confirm';
  }

}
