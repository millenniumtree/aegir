@platforms @revisions @api @wip @disabled
Feature: Create and manage Aegir platforms
  In order to define platforms that can be combined into applications,
  as a platform manager,
  I want to be able to manage Aegir platform entities.

  Background:
    Given I run "drush -y en aegir_test_platform"
      And I am logged in as a "Platform manager"
      And I am on "admin/aegir/platforms"

  Scenario: Create a platform for later scenarios
    Given I click "Add Ægir platform"
      And I click "TEST_PLATFORM_TYPE_1" in the "content" region
      And I fill in "Name" with "TEST_PLATFORM_B"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_PLATFORM_B platform."
      And I should see the link "Revisions" in the "tabs" region

  Scenario: Update the platform to create some revisions
    Given I should see the link "TEST_PLATFORM_B" in the "content" region
      And I click "TEST_PLATFORM_B"
     When I fill in "Name" with "TEST_PLATFORM_B-changed"
      And I check the box "Create new revision"
      And for "Revision log message" I enter "First test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_PLATFORM_B-changed platform."
     When I click "Edit" in the "tabs" region
      And I fill in "Name" with "TEST_PLATFORM_B"
      And I check the box "Create new revision"
      And for "Revision log message" I enter "Second test log message."
      And I press the "Save" button
      And I click "Revisions"
     Then I should see the heading "Revisions for TEST_PLATFORM_B" in the "header" region
      And I should see the text "First test log message."
      And I should see the text "Second test log message."

  Scenario: View a platform revision
    Given I click "TEST_PLATFORM_B"
      And I click "Revisions"
     When I click "view revision" in the "First test log message." row
     Then I should see "Revision of TEST_PLATFORM_B-changed from" in the "header" region

  Scenario: Revert a platform revision
    Given I click "TEST_PLATFORM_B"
      And I click "Revisions"
     When I click "Revert" in the "First test log message." row
     Then I should see the text "Are you sure you want to revert to the revision from" in the "header" region
     When I press "Revert"
     #Then I should see the success message "The platform TEST_PLATFORM_B has been reverted to the revision from"


