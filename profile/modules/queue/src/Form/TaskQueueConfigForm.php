<?php

namespace Drupal\aegir_queue\Form;

use Drupal\aegir_api\Logger\AegirLoggerTrait;
use Drupal\aegir_queue\TaskQueue\TaskQueueInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\config_update\ConfigRevertInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TaskQueueConfigForm.
 *
 * @package Drupal\aegir_queue
 */
class TaskQueueConfigForm extends ConfigFormBase {

  use AegirLoggerTrait;

  /**
   * We provide a simple method to revert configuration to default values.
   *
   * @var ConfigReverter
   */
  protected $reverter;

  /**
   * The task queue.
   *
   * @var Drupal\aegir_queue\TaskQueue\TaskQueueInterface
   */
  protected $queue;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ConfigRevertInterface $reverter, TaskQueueInterface $queue) {
    parent::__construct($config_factory);
    $this->reverter = $reverter;
    $this->queue = $queue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the config factory service that we'll pass along to the parent
      // constructor.
      $container->get('config.factory'),
      // Load the config reverter service.
      $container->get('config_update.config_update'),
      // Load the task queue service.
      $container->get('aegir_queue.task_queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return $this->queue->getConfigNames();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_queue_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['connection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Task queue connection settings'),
    ];
    foreach ($this->queue->getConnectorForm() as $name => $element) {
      $form['connection'][$name] = $element;
    };
    $form['connection']['check'] = [
      '#type' => 'submit',
      '#value' => $this->t('Check connection settings'),
    ];
    $form['connection']['queue_valid'] = [
      '#type' => 'submit',
      '#value' => $this->t('Check task queue'),
    ];

    $form = parent::buildForm($form, $form_state);
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset to defaults'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#id'] != 'edit-reset') {
      if ($this->queue->checkConnectorSettings($form_state) === FALSE) {
        // @TODO Add additional debug instructions (paths to backend logs, etc.)
        $form_state->setErrorByName('connection', $this->t('Verify connection settings.'));
        $this->log()->error('The configuration options have NOT been saved.');
        return FALSE;
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    switch ($form_state->getTriggeringElement()['#attributes']['data-drupal-selector']) {
      case 'edit-reset':
        $this->resetForm();
        break;

      case 'edit-submit':
        $this->queue->saveConnectorSettings($form_state);
        parent::submitForm($form, $form_state);
        break;

      case 'edit-check':
        // Check occurs in validateForm().
        break;

      case 'edit-queue-valid':
        $this->checkQueueValidity('relayd');
        $this->checkQueueValidity('dispatcherd');
        break;

      default:
        break;
    }
  }

  /**
   * Send a queue_valid task to either relayd or dispatcherd worker to validate
   * they can successfully pass data back by running "drupal aegir" commands.
   *
   * @param string $worker
   *   The name of the queue worker to send the task to (relayd or dispatcherd)
   */
  protected function checkQueueValidity(string $worker) {

    // Initially set the queue valid state to FALSE
    $queue_valid = FALSE;
    $state_var = 'aegir.'. $worker .'.queue_valid';
    $state = \Drupal::state();
    $state->set($state_var, $queue_valid);

    // Post a task to set queue valid state to TRUE
    $task_name = $worker .'.queue_valid';
    $task = $this->queue->addTask($task_name, [$worker], $worker);

    // Poll for the queue_valid state to be set
    $increment = 0.1; // .1 seconds
    $counter = 0;
    $timeout = 60; // 50 seconds
    while (!$queue_valid) {
      usleep($increment * 1000000);
      $counter += $increment;
      if ($counter >= $timeout) {
        $this->log()->error('Task %task_name execution exceeded timeout of :timeout seconds checking queue validity for %worker. Check %path on the %worker server for more details.', [
          '%task_name' => $task_name,
          ':timeout' => $timeout / 1000000,
          '%worker' => $worker,
          '%path' => '/var/log/aegir/' . $worker . '.error.log',
        ]);
        return FALSE;
      }

      $state->resetCache();
      $queue_valid = $state->get($state_var);
      # TODO: consider cleaning up the state var here
    }

    $this->log()->success('The %worker task queue is functioning correctly. Round-trip took :seconds seconds.', [
      '%worker' => $worker,
      ':seconds' => $counter,
    ]);
  }

  /**
   * Revert form values to defaults.
   */
  protected function resetForm() {
    foreach ($this->getEditableConfigNames() as $config_name) {
      if (!$this->reverter->revert('system.simple', $config_name)) {
        // @codeCoverageIgnoreStart
        $this->log()->error('Failed to reset configuration (:name) to default values.', [
          ':name' => $config_name,
        ]);
        return FALSE;
        // @codeCoverageIgnoreEnd
      }
    }
    $this->log()->success('The configuration options have been reset to default values.');
  }

}
