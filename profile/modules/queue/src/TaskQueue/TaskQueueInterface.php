<?php

namespace Drupal\aegir_queue\TaskQueue;

use Drupal\aegir_queue\Connector\ConnectorInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface TaskQueueInterface.
 *
 * @package Drupal\aegir_queue
 */
interface TaskQueueInterface {

  /**
   * Constructs a TaskQueue object.
   *
   * @param \Drupal\aegir_queue\Connector\ConnectorInterface $connector
   *   The connector to the queue.
   */
  public function __construct(ConnectorInterface $connector);

  /**
   * Validate connector settings from configuration form.
   *
   * Check that form values for the connector will allow a connection to the
   * task queue.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormState passed from the TaskQueueConfigForm.
   */
  public function checkConnectorSettings(FormStateInterface $form_state);

  /**
   * Add a task to the queue.
   *
   * @param string $type
   *   The type of the task to add to the queue.
   * @param array $args
   *   An array of arguments for the task.
   * @param string $queue
   *   The name of a Celery queue to which to connect.
   */
  public function addTask(string $type, array $args, string $queue);

}
