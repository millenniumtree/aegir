<?php

namespace Drupal\aegir\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Tests Aegir installation profile expectations.
 *
 * @group aegir
 */
class AegirTest extends WebTestBase {

  protected $profile = 'aegir';

  /**
   * Disable schema validation for the base theme.
   *
   * @see: https://www.drupal.org/node/2860072
   */
  protected function getConfigSchemaExclusions() {
    return array_merge(parent::getConfigSchemaExclusions(), ['bootstrap.settings']);
  }

  /**
   * Tests aegir installation profile.
   */
  public function testAegir() {
    // Create a user to test tools and navigation blocks for logged in users
    // with appropriate permissions.
    $user = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($user);
    $this->drupalGet('admin');
    $this->assertText(t('Administer settings'));

    // Ensure that there are no pending updates after installation.
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('update.php/selection');
    $this->assertText('No pending updates.');

    // Ensure that there are no pending entity updates after installation.
    $this->assertFalse($this->container->get('entity.definition_update_manager')->needsUpdates(), 'After installation, entity schema is up to date.');
  }

}
