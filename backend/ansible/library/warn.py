#!/usr/bin/python

from ansible.module_utils.basic import AnsibleModule


def run_module():
    '''
    Leverage Ansible's custom stats, to register an aggregated count of warnings per host.
    We then look the stat up in a custom callback plugin (see: callback_plugins/aegir_output.py).
    '''

    module_args = dict(
        msg=dict(type='str', required=True)
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=False
    )

    result = {
        'warning': True,
        'ansible_stats': {
            'data': {
                '_warnings': 1
            },
            'per_host': True,
            'aggregate': True
        }
    }

    module.warn(module.params['msg'])

    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
