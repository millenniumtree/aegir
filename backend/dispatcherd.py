from celery import Celery
from celery.utils.log import get_task_logger
import jinja2
import json
import os
import shlex
import subprocess
import tempfile
import time
import urllib
import urllib.request as urllib2
import yaml

dispatcherd = Celery('aegir', broker='pyamqp://aegir:53cr3t!@rabbitmq//')

dispatcherd.conf.update(
    task_serializer='json',
    result_serializer='json',
    result_backend = 'redis://redis/0',
)

log = get_task_logger(__name__)

@dispatcherd.task(queue='dispatcherd')
def echo(text):
    ''' Dummy task to test connections to the queue. '''
    log.info('Received echo task: "' + text + '"')
    return text

@dispatcherd.task(queue='dispatcherd')
def queue_valid(worker):
    ''' Trigger relayd to run aegir:validate_queue and set queue_valid state to TRUE.  '''
    log.info('Received queue_valid task. Sending to relayd...')
    dispatcherd.send_task('relayd.queue_valid', queue='relayd', args=[worker])
    log.info('Queue valid task sent to relayd.')

@dispatcherd.task(queue='dispatcherd')
def relay_aegir_input(config, data):
    ''' Trigger relayd to run aegir:input with the provided config and data. '''
    log.info('Sending data to relayd to update front-end.')
    dispatcherd.send_task('relayd.aegir_input', queue='relayd', args=[config, data])
    log.info('Sent data to relayd to update front-end.')

@dispatcherd.task(queue='dispatcherd')
def relay_aegir_log(config, data):
    ''' Trigger relayd to run aegir:log with the provided operation type and data. '''
    log.info('Sending log data to relayd to update front-end.')
    dispatcherd.send_task('relayd.aegir_log', queue='relayd', args=[config, data])
    log.info('Sent log data to relayd to update front-end.')

@dispatcherd.task(queue='dispatcherd')
def relay_aegir_exitcode(config, exitcode):
    ''' Trigger relayd to run aegir:exitcode with the provided operation. '''
    log.info('Sending exitcode to relayd to update front-end.')
    dispatcherd.send_task('relayd.aegir_exitcode', queue='relayd', args=[config, exitcode])
    log.info('Sent exitcode to relayd to update front-end.')

@dispatcherd.task
def ansible_debug(config, variables):
    ''' Run ansible to debug config and variables. '''
    env = get_ansible_env(config)
    debug = { 'config': config, 'variables': variables, 'env': env }
    cmd = get_ansible_debug_cmd(debug)
    run_cmd(cmd, env, config)

@dispatcherd.task
def ansible(config, variables):
    ''' Run ansible-playbook with specified roles and variables. '''
    env = get_ansible_env(config)
    playbook = get_playbook(config)
    # Add a list of the variables we're writing.
    variables['aegir_uuids'] = config['uuids']
    variables['aegir_vars'] = list(variables.keys())
    # Add a list of the related entity UUIDs.
    var_file = get_var_file(variables)
    cmd = get_ansible_cmd(config, playbook, var_file)
    log.info('Running command: ' + cmd)
    run_cmd(cmd, env, config)
    # @todo Clean up temporary playbook, var_file, etc.?

@dispatcherd.task
def drupal_login(context):
    ''' Generate and return a Drupal one-time login link. '''
    site_path = context['field_platform_path'] + '/sites/' + context['field_drupal_vhost_url']
    # @TODO Add error-checking, find best path to Drush, etc.
    # @TODO Determine whether it'd be safe to turn this into an arbitrary Drush command plus args.
    drush_cmd = '/var/aegir/.local/bin/drush8 uli';
    return subprocess.check_output(drush_cmd, cwd=site_path, shell=True) + '?destination=/'

def get_ansible_env(config):
    env = {
        'ANSIBLE_CONFIG': '/app/backend/ansible/ansible.cfg',
        'ANSIBLE_ROLES_PATH': config['roles_path'],
        'HOME': '/var/aegir',
        'PYTHONUNBUFFERED': '1',
    }
    return env

def get_ansible_debug_cmd(variables):
    cmd = '/usr/local/bin/ansible localhost '
    cmd += '--inventory localhost, '
    cmd += '--connection local '
    cmd += '--module-name debug '
    cmd += '--args var=debug '
    cmd += '--extra-vars \"{}\"'.format('debug=\\"' + json.dumps(variables) + '\\"')
    #cmd += '--extra-vars \\"{}\\"'.format('debug=' + json.dumps(variables))
    return cmd

def get_ansible_cmd(config, playbook, var_file):
    cmd = '/usr/local/bin/ansible-playbook '
    cmd += '--inventory localhost, '
    cmd += '--extra-vars @' + var_file.name
    cmd += ' ' + playbook.name
    return cmd

def get_playbook(config):
    playbook = get_playbook_content(config)
    temp_file = write_temp_file(playbook)
    return temp_file

def get_playbook_content(config):
    tpl_path = '/app/backend/ansible/playbook.yml.j2'
    playbook = render(tpl_path, config)
    return playbook

def write_temp_file(content):
    temp = tempfile.NamedTemporaryFile(delete=False, suffix='.yml')
    temp.write(bytes(content, 'ascii'))
    temp.close()
    return temp

def get_var_file(variables):
    var_file = yaml.dump(variables)
    temp_file = write_temp_file(var_file)
    return temp_file

def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(path or './')
    ).get_template(filename).render(context)

def run_cmd(cmd, env, config):
    ''' Run a system command and post the output to the frontend. '''
    cmd = shlex.split(cmd)
    seq = 0
    post_log('Running command: ' + ' '.join(cmd), seq, config)
    try:
        process = subprocess.Popen(cmd,
                             env=env,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             bufsize=1)
        timeout = 100
        current_time = time.time() * 1000
        flush_time = current_time + timeout
        lines = ''
        for line in iter(process.stdout.readline, b''):
            lines += line.decode()
            current_time = time.time() * 1000
            if current_time > flush_time:
                flush_time = current_time + timeout
                if lines:
                    seq += 1
                    post_log(lines, seq, config)
                    lines = ''
        post_log(lines, seq, config)
        process.stdout.close()
        exitcode = process.wait()
        post_exitcode(exitcode, config)
    except RuntimeError as e:
        post_log('Execution failed:' + e.strerror, seq+1, config)
        post_exitcode(e.errno, config)

def post_log(output, seq, config):
    ''' Send log output to relayd for the frontend.'''

    log_data = {
      'log_timestamp': time.time(),
      'log_output': output,
      'log_sequence': seq,
    }

    relay_aegir_log(config, log_data)

# TODO: re-implement this using the celery queue to post data to aegir:input command (@see hostmasterd)
def post_exitcode(exitcode, config):
    ''' Register an exit code with the frontend. '''
    relay_aegir_exitcode(config, exitcode)
