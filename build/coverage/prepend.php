<?php

use SebastianBergmann\CodeCoverage\CodeCoverage;
use SebastianBergmann\CodeCoverage\Report\PHP;

aegir_code_coverage_before();

/**
 * Enable code coverage, and register a shutdown function to write out the
 * coverage data for this process.
 */
function aegir_code_coverage_before() {
  global $coverage;

  $aegir_platform_path = getenv('AEGIR_PLATFORM_PATH') ? getenv('AEGIR_PLATFORM_PATH') : '/var/aegir/platforms/aegir';

  $dir = $aegir_platform_path . '/vendor/';

  require_once $aegir_platform_path . '/vendor/autoload.php';
  // We need to manually include each file here since
  // AnnotationRegistry::registerLoader() appears to choke on running the
  // autoload twice.
  #require_once $dir . 'phpunit/php-file-iterator/src/Iterator.php';
  #require_once $dir . 'phpunit/php-file-iterator/src/Factory.php';
  #require_once $dir . 'phpunit/php-file-iterator/src/Facade.php';
  #require_once $dir . 'sebastian/code-unit-reverse-lookup/src/Wizard.php';
  #require_once $dir . 'phpunit/php-code-coverage/src/Filter.php';
  #require_once $dir . 'phpunit/php-code-coverage/src/Driver/Driver.php';
  #require_once $dir . 'phpunit/php-code-coverage/src/Driver/Xdebug.php';
  #require_once $dir . 'phpunit/php-code-coverage/src/CodeCoverage.php';
  #require_once $dir . 'phpunit/php-code-coverage/src/Report/PHP.php';
  #require_once $dir . 'sebastian/environment/src/Runtime.php';

  $coverage = new CodeCoverage();
  $coverage->setCheckForUnexecutedCoveredCode(TRUE);
  $coverage->setAddUncoveredFilesFromWhitelist(TRUE);
  #$coverage->setProcessUncoveredFilesFromWhitelist(TRUE);

  $profile_path = $aegir_platform_path . '/profiles/aegir';
  $suffixes = ['.php', '.module', '.install', '.inc', '.theme'];
  foreach ($suffixes as $suffix) {
    $coverage->filter()->addDirectoryToWhitelist($profile_path, $suffix);
  }

  register_shutdown_function('aegir_code_coverage_after');

  $coverage->start('aegir');
}

/**
 * Generate a uniquely-named PHP-serialized report.
 *
 * We then merge these later via `phpcov merge`.
 */
function aegir_code_coverage_after() {
  global $coverage;

  $coverage->stop();

  $project_dir = getenv('CI_PROJECT_DIR') ? getenv('CI_PROJECT_DIR') : '/vagrant';
  $php_report_path = $project_dir . '/build/coverage/php';
  $php_report_file = $php_report_path . '/index-' . uniqid() . '.cov';

  $result = new PHP();
  $result->process($coverage, $php_report_file);
}
