---
title: Concepts
weight: 10
---

## Deployment Target

VM or Kubernetes cluster set up to host an Application/Project instance

## Project

* A **Project** is a "developer mode" concept that models a codebase (usually a git repository) which may have multiple distinct environments deployed (eg. feature branch, staging/qa)
    * This is the mode we plan to focus on for now, as it's a dependency of the Application mode
    * By default branch environments get rolling updates that don't require blue/green, backup/restore, monitoring/rollback functionality

## Application

* An **Application** is an "operator mode" concept that models some form of versioned release process. Applications:
    * Could be an "upstream" codebase like a Drupal install profile (eg. OpenSocial), where I don't control releases directly
    * Could also be based on a custom-built codebase (eg. a Drupal 10 singleton site), usually associated with a Project
    * Orient toward "herds" of sites running on them, and a singleton instance is just a degenerate case
    * Have different versions (and the concept of a "latest" version), and facilitate moving herds of sites between them
      * As a policy, you could have a `production` environment doing CD eg. no staged release model
      * Alternately, you could clone and set up a test environment on the new version, wait for approval to cut over the live environment
    * In Aegir3, the concept of Distributions modeled this:
        * Check upstream application for a new version, and if so:
            * Create a new Platform
            * Notify the admin for any sites on the original version
            * Run a series of migrations to move sites to latest version
            * Distributions gave us the concept of "the latest version"

## Release

A specific version/tag of a Project/Application, along with dependencies (docker image)

## Branch Environment

A space to hold a Deployment (in k8s context, these are 1:1, ie. , so maybe Aegir treats them as one thing?)

  * **Deployment?** An instance of a site deployed to a Deployment Target
  * Could be a "branch" or "live/release" (or other?) type of Environment

## Release Environment

A Branch Environment based on an Application Release?
