---
title: Personas
weight: 1
---

## Personas

These are higher level perspectives that Aegir users take on:

* Drupal site-builder: a user with a drupal project
* Aegir site-hoster: an Aegir user with a Project running on Aegir
* Aegir5 Application hoster: an Aegir user with an Application deployed on Aegir
* Aegir5 Infrastructure engineer: systems engineer needing to manage Deployment Targets
* SaaS Product Developer: software developer needing to build and manage a SaaS Product

## System Roles

These are roles within the system, which are composable into the Personas
above (more details TBD):

* Aegir user: an Aegir user with a Project running on Aegir
* Aegir manager:
* Aegir administrator:
