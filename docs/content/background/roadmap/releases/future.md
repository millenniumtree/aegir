---
title: Future Releases
weight: 100
---

These features are not actively on the roadmap, but we plan to slot them in as
their relative priorities become clear.

* [Host a Drupal Project (K8S Deployment Target, kubectl backend)](https://gitlab.com/groups/aegir/-/epics/14)
* [Host a Drupal Project (K8S Deployment Target, Helm backend)](https://gitlab.com/groups/aegir/-/epics/15)
* [Deploy Aegir5 to a Deployment Target](https://gitlab.com/groups/aegir/-/epics/10)
* [Host a Drupal Project (VM Deployment Target, Shell backend?)](https://gitlab.com/groups/aegir/-/epics/16)
* [Provision cloud/backend resources for Aegir](https://gitlab.com/groups/aegir/-/epics/17)
* [Herds of Applications](https://gitlab.com/groups/aegir/-/epics/18)
