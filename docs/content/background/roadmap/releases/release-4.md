---
title: Release 4
weight: 40
---

Theme: [Application](../../concepts#application) workflows

[Roadmap Request For Comment: Release 4 User stories](https://gitlab.com/groups/aegir/-/epics/4) ([milestone](https://gitlab.com/groups/aegir/-/milestones/4))

* [As an *Aegir5 site-builder*, I want to **create a live environment** from my Project](https://gitlab.com/aegir/aegir/-/issues/156):
* [As an *Aegir5 site-hoster*, I want to **update my live environment** with a new release](https://gitlab.com/aegir/aegir/-/issues/157):
* [As an *Aegir5 site-hoster*, I want to host a site based on an Application (pre-baked Distribution)](https://gitlab.com/aegir/aegir/-/issues/158):
* [As an Aegir5 Application hoster, I want to upgrade my site based on an Application release](https://gitlab.com/aegir/aegir/-/issues/159)
* [As an *Aegir5 site-hoster*, I want to to "migrate" my site to the latest version of an Application](https://gitlab.com/aegir/aegir/-/issues/160)
