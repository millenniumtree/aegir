---
title: Release 3
weight: 30
---

Theme: [Project](../../concepts#project) lifecycle support

[Roadmap Request For Comment: Release 3 User stories](https://gitlab.com/groups/aegir/-/epics/3) ([milestone](https://gitlab.com/groups/aegir/-/milestones/3))

* [Prepare a Drupal site archive from Aegir3](https://gitlab.com/groups/aegir/-/epics/25)
* [Import a Drupal site archive](https://gitlab.com/groups/aegir/-/epics/24)
* [Rollback a Project Environment (to a previous Snapshot)](https://gitlab.com/groups/aegir/-/epics/23)
* [Snapshot a Project Environment](https://gitlab.com/groups/aegir/-/epics/22)
* [Restore a Project Environment (from a Backup)](https://gitlab.com/groups/aegir/-/epics/21)
* [Backup a Project Environment](https://gitlab.com/groups/aegir/-/epics/20)
* [Update a Project Environment](https://gitlab.com/groups/aegir/-/epics/19)
