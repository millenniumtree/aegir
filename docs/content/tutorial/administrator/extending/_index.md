---
title: Extending Ægir

---

Ægir is designed to make it straight-forward to add support for hosting new applications. To illustrate this, let's add support for Hugo, a popular static site generator. Hugo builds stand-alone sites, and doesn't support multi-site scenarios. This simplifies things, as we won't need a separate platform type.

**NB** This example is incomplete, specifically lacking details in how to implement and connect backend components after creating the Site Type, Operation and Task entities in the frontend.

{{< children >}}

