---
title: Queue System Components
menutitle: Components
weight: 10
---

## Queue mechanism components

@TODO: expand on these components and how they fit together.

* [ ] Docker Compose file(s); eg. [.ddev/docker-compose.dispatcherd.yml](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/.ddev/docker-compose.dispatcherd.yml)
* [ ] Docker image(s); eg.
    * [ ] [Packer build config](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/build/packer/docker/dispatcherd.json)
    * [ ] Provisioning scripts:
        * [ ] [Shell](https://gitlab.com/aegir/aegir/-/tree/68-implement-ansible-module-for-logging/build/packer/scripts)
        * [ ] Ansible:
            * [ ] [Ansible config file](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/build/ansible/ansible.cfg)
            * [ ] [Ansible inventory file](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/build/ansible/hosts)
            * [ ] [Playbook](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/build/ansible/dispatcherd.yml)
            * [ ] [Role](https://gitlab.com/aegir/aegir/-/tree/68-implement-ansible-module-for-logging/build/ansible/roles/aegir.workers)
* [ ] [DDEV command to re-deploy and re-start the queue worker](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/.ddev/commands/dispatcherd/dispatcherd)
* [ ] [Make targets for Packer image builds](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/drumkit/mk.d/20_ci.mk)
* [ ] [Make targets for DDEV](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/drumkit/mk.d/20_ddev.mk) (especially pulling new images)
* [ ] Incorporate these images into CI as services
* [ ] document the Drupal console commands `aegir:input`, `aegir:log`, and `aegir:exitcode` and how they interact with relayd/dispatcherd


