---
title: Queue Validation
menutitle: Validation
weight: 20
---

## Queue Validation mechanisms

In [Issue #64](https://gitlab.com/aegir/aegir/-/issues/64), we implemented the
`drupal aegir:validate_queue` command (see commits
[ab3b2d92](https://gitlab.com/aegir/aegir/-/commit/ab3b2d92182a2a882099f7276fa13189e6d8e9b4),
[7d76e431](https://gitlab.com/aegir/aegir/-/commit/7d76e4310441be49f90580256c2f4704f6cc749c),
[f061785c](https://gitlab.com/aegir/aegir/-/commit/f061785c17251259bcabcb6c2e921d80d4ba3fa0))
along with some testing/debugging mechanisms currently living alongside the
"Check connection settings" page (`admin/aegir/queue`), called "Check task
queue". It works like this:

1. The [form validation code](https://gitlab.com/aegir/aegir/-/blob/64-implement-frontend-worker-daemon/profile/modules/queue/src/TaskQueue/CeleryTaskQueue.php#L27) simply confirms that the 2 Celery workers are up and running, by posting an "echo" task to each of them, and waiting for them to respond directly (see links to relayd.py and dispatcherd.py `@task.echo` functions)
    * note this is the same routine that runs when you click "Check connection settings", to first validate the workers are up
2. The [new TaskQueueConfigForm::checkQueueValidity() form submit handler](https://gitlab.com/aegir/aegir/-/blob/64-implement-frontend-worker-daemon/profile/modules/queue/src/Form/TaskQueueConfigForm.php#L150) actually posts a Celery task onto the queue, which we've called `queue_valid`. Here again, we've implemented both a [`relayd.queue_valid`](https://gitlab.com/aegir/aegir/-/blob/64-implement-frontend-worker-daemon/backend/relayd.py#L26) and [`dispatcherd.queue_valid`](https://gitlab.com/aegir/aegir/-/blob/64-implement-frontend-worker-daemon/backend/dispatcherd.py#L30)] routine for each of the workers, and they are triggered in turn. Both use the same technique: set a [State API](https://www.drupal.org/docs/8/api/state-api/overview) variable to `FALSE` initially, then call the worker task whose job is to end up setting that same variable to `TRUE`. After posting the task, the submit handler currently polls the State variable (resetting the cache each time through the wait loop), and returns TRUE when the State variable changes, or FALSE if it times out.
    * In the case of the `relayd.queue_valid` task, we simply call `drupal aegir:queue_valid` immediately, validating that we can have the Python Celery task code call out to a `drupal aegir` command, in turn feeding data back to the frontend Aegir site.
    * In the case of the `dispatcherd.queue_valid` task, we emulate the "round trip" feedback mechanism, where a backend task in turn posts a Celery task onto the queue for `relayd` to pick up and process (generally via a `drupal aegir` console command). In this case, we `dispatcherd.validate_queue` posts a `relayd.validate_queue`, which in turn calls the `drupal aegir:queue_valid` command, just as in the previous step.

