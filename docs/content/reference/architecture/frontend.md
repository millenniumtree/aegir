---
title: Front-end
weight: 10
---


## Front-end

The front-end is written in Drupal 8. It consists of some base entities and traits, along with basic admin interfaces for creating and managing fields and bundles.

Beyond the low-level [Operations and Tasks](../operations-tasks) which form the
building blocks for the user interface in Aegir5, there are a set of
higher-level entities in the front-end UI:

### Deployment Targets (Clusters, VMs)

In a traditional Aegir3 model, we have Servers representing a location where
sites can be deployed into. In a Kubernetes context, this would be a cluster.
Deployment Targets are the frontend concept that encompasses both of these.

We intend to support provisioning of Deployment Targets in the future. For the
time-being, we will simply be pointing at existing Deployment Targets to which
we will want to deploy our applications.

### Projects (codebase)

Projects represent a codebase at a more abstract level. Whereas Releases (see
below) are fully instantiated codebases, Projects represent the source of the
codebase itself; usually a Git repository.

### Applications (distribution)

Applications represent a pre-packaged or upstream "distribution" of an
application with a versioned release process. Aegir



### Releases (image)

Releases are basically the equivalent of Platforms in Aegir 3. They capture the
runtime environment, with a specific version of our codebase deployed and all
dependencies downloaded. In Kubernetes, these are basically container images.

Sharing a common codebase across multiple sites can be achieved by leveraging
the same Release. However, unlike Platforms, we are not intending to support
Drupal’s multisite capability in Aegir5.

Multisite adds a lot of complexity, especially when running in a containerized
environment. It also introduces multiple security and stability concerns.

### Deployments (site instances)

Environments become the equivalent of Sites in Aegir 3. They capture the
specific configuration required to run an instance of a specific Release of the
Project (or Application), along with the storage required for both the database
and any uploaded files.

In a Kubernetes context, a Deployment would consist of an Environment
(namespace + storage), combined with an Deployment resource (ingress, drupal
pod, etc).
