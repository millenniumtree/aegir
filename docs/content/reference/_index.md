---
title: Technical reference
weight: 30
---

This section provides technical descriptions of the project architecture and various components.

{{< children >}}
